<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::post('register', 'Auth\ApiAuthController@register');
//Route::post('login', 'Auth\ApiAuthController@login');
//

Route::group(['prefix' => 'admin'], function(){
    Route::apiResource('sellers', 'Admin\SellerController');
});

Route::apiResource('stores', 'StoreController');
Route::apiResource('products', 'ProductController');

Route::get('payment-submit', 'PaymentController@submit')->name('payment-submit');
