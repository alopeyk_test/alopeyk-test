<?php

namespace App\Contracts;


use App\Exceptions\Products\CreateException;
use App\Exceptions\Products\DeleteException;
use App\Exceptions\Products\UpdateException;
use App\Product;

/**
 * Class ProductRepository
 * @package App\Repositories
 */
interface ProductRepositoryContract
{
    /**
     * Get all products.
     *
     * @return Product[]
     */
    public function all();

    /**
     * Get all products by GeoData.
     * GeoData = [latitude, longitude, radius].
     *
     * @param $geoData
     * @return mixed
     */
    public function getByGeo($geoData);

    /**
     * Find a specific Product by id.
     *
     * @param $id
     * @return mixed
     */
    public function find($id);

    /**
     * Store new Product instance.
     *
     * @param $data
     * @return mixed
     * @throws CreateException
     */
    public function create($data);

    /**
     * Update an existing product.
     *
     * @param $product_id
     * @param $data
     * @return mixed
     * @throws UpdateException
     */
    public function update($product_id, $data);

    /**
     * Destroy an specified product.
     *
     * @param $product_id
     * @return mixed
     * @throws DeleteException
     */
    public function destroy($product_id);
}
