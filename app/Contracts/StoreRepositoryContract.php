<?php

namespace App\Contracts;


use App\Exceptions\Store\CreateException;
use App\Exceptions\Store\DeleteException;
use App\Exceptions\Store\UpdateException;
use App\Store;

/**
 * Class StoreRepository
 * @package App\Repositories
 */
interface StoreRepositoryContract
{
    /**
     * List all stores.
     *
     * @return Store[]
     */
    public function all();

    /**
     * List stores in a specific GeoData.
     * geoData = [latitude, longitude, radius].
     *
     * @param $geoData
     * @return mixed
     */
    public function getByGeo($geoData);

    /**
     * Find a specific Store.
     *
     * @param $id
     * @return mixed
     */
    public function find($id);

    /**
     * Create a new Store.
     *
     * @param $data
     * @return mixed
     * @throws CreateException
     */
    public function create($data);

    /**
     * Update an existing Store.
     *
     * @param $store_id
     * @param $data
     * @return bool
     * @throws UpdateException
     */
    public function update($store_id, $data);

    /**
     * Delete an specific store.
     *
     * @param $store_id
     * @return mixed
     * @throws DeleteException
     */
    public function delete($store_id);
}
