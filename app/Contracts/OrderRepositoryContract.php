<?php

namespace App\Contracts;


/**
 * Class OrderRepository
 * @package App\Repositories
 */
interface OrderRepositoryContract
{
    /**
     * Buy new Product from a specific store.
     *
     * @param $user_id
     * @param $product_id
     * @param $store_id
     * @param int $quantity
     * @return mixed
     */
    public function make($user_id, $product_id, $store_id, $quantity = 1);
}
