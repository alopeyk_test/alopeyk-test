<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['name', 'price', 'quantity', 'store_id', 'status'];

    public function store()
    {
        return $this->belongsTo(Store::class);
    }
}
