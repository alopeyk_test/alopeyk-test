<?php

namespace App\Http\Controllers;

use App\Contracts\ProductRepositoryContract;
use App\Exceptions\NotAllowedException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

/**
 * Class ProductController
 * @package App\Http\Controllers
 */
class ProductController extends BaseController
{
    /**
     * @var ProductRepositoryContract
     */
    private $product;

    /**
     * ProductController constructor.
     * @param ProductRepositoryContract $product
     */
    public function __construct(ProductRepositoryContract $product)
    {
        $this->product = $product;
    }

    /**
     * Display a listing of all products..
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = $this->product->all();

        return $this->sendResponse(
            $products,
            'Products retrieved successfully.'
        );
    }

    /**
     * Display a list of products in a GeoLocation.
     * GeoData = [Latitude, longitude, radius].
     *
     * @param  $geoData
     * @return \Illuminate\Http\Response
     */
    public function getByGeo($geoData)
    {
        $products = $this->product->getByGeo($geoData);

        return $this->sendResponse(
            $products,
            'Nearby Products retrieved successfully.'
        );
    }

    /**
     * Display the specified product.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = $this->product->find($id);

        return $this->sendResponse(
            $product,
            'Specified product retrieved successfully.'
        );
    }

    /**
     * Create a new product.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * @throws NotAllowedException
     */
    public function create(Request $request)
    {
        if (!Gate::allows('create-product')) {
            throw new NotAllowedException();
        }

        $result = $this->product->create($request->input());

        return $this->sendResponse(
            $result,
            'Product created successfully.'
        );
    }
    /**
     * Update the specified product.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * @throws NotAllowedException
     */
    public function update(Request $request, $id)
    {
        if (!Gate::allows('update-product')) {
            throw new NotAllowedException();
        }

        $result = $this->product->update($id, $request->input());

        return $this->sendResponse(
            $result,
            'Product updated successfully.'
        );
    }

    /**
     * Remove the specified product.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!Gate::allows('delete-product')) {
            throw new NotAllowedException();
        }

        $result = $this->product->delete($id);

        return $this->sendResponse(
            $result,
            'Products deleted successfully.'
        );
    }
}
