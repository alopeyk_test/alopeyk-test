<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class SellerController extends Controller
{
    public function create(Request $request)
    {
        return User::create($request->input());
    }
}
