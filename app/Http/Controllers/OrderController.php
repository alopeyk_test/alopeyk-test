<?php

namespace App\Http\Controllers;

use App\Contracts\OrderRepositoryContract;
use App\Services\PaymentGateway\AlopeykGateway;
use Illuminate\Http\Request;

/**
 * Class OrderController
 * @package App\Http\Controllers
 */
class OrderController extends Controller
{
    /**
     * @var OrderRepositoryContract
     */
    private $order;
    /**
     * @var mixed
     */
    private $gateway;

    /**
     * OrderController constructor.
     * @param OrderRepositoryContract $order
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function __construct(OrderRepositoryContract $order)
    {
        $this->order = $order;
        $this->gateway = app()->make(AlopeykGateway::class);
    }

    /**
     * Buy new Product from a specific store.
     *
     * @param $product_id
     * @param $store_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function make($product_id, $store_id)
    {
        $user_id = Auth::user()->id();
        if ($new_order = $this->order->make($user_id, $product_id, $store_id)) {
            $transaction = $this->gateway->create($new_order);

            return redirect()->route('payment-submit', $transaction);
        }
    }
}
