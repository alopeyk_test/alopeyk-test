<?php

namespace App\Http\Controllers;

use App\Exceptions\NotAllowedException;
use App\Repositories\StoreRepository;
use Illuminate\Http\Request;
use App\Store;
use Illuminate\Support\Facades\Gate;

class StoreController extends Controller
{
    /**
     * @var StoreRepository
     */
    private $store;

    /**
     * StoreController constructor.
     * @param StoreRepository $store
     */
    public function __construct(StoreRepository $store)
    {
        $this->store = $store;
    }

    /**
     * Display a listing of the products.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stores = $this->store->all();

        return $this->sendResponse(
            $stores,
            'Stores retrieved successfully.'
        );
    }

    /**
     * Display a list of stores in a GeoLocation.
     * GeoData = [Latitude, longitude, radius].
     *
     * @param $geoData
     * @return mixed
     */
    public function getByGeo($geoData)
    {
        $stores = $this->store->getByGeo($geoData);

        return $this->sendResponse(
            $stores,
            'Nearby Stores retrieved successfully.'
        );
    }

    /**
     * Display the specified store.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $store = $this->store->find($id);

        return $this->sendResponse(
            $store,
            'Specified Store retrieved successfully.'
        );
    }

    /**
     * Store a newly created store.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!Gate::allows('create-store')) {
            throw new NotAllowedException();
        }

        $result = $this->store->create($request->input());

        return $this->sendResponse(
            $result,
            'Store created successfully.'
        );
    }

    /**
     * Update the specified store.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!Gate::allows('update-store')) {
            throw new NotAllowedException();
        }

        $result = $this->store->update($id, $request->input());

        return $this->sendResponse(
            $result,
            'Store updated successfully.'
        );
    }

    /**
     * Remove the specified store.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!Gate::allows('delete-store')) {
            throw new NotAllowedException();
        }

        $result = $this->store->delete($id);

        return $this->sendResponse(
            $result,
            'Store deleted successfully.'
        );
    }
}
