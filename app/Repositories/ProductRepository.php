<?php


namespace App\Repositories;

use App\Contracts\ProductRepositoryContract;
use App\Exceptions\Products\CreateException;
use App\Exceptions\Products\DeleteException;
use App\Exceptions\Products\UpdateException;
use App\Product;

/**
 * Class ProductRepository
 * @package App\Repositories
 */
class ProductRepository implements ProductRepositoryContract
{
    private $product;

    /**
     * ProductRepository constructor.
     * @param Product $product
     */
    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    /**
     * Get all products.
     *
     * @return Product[]
     */
    public function all() {
        return $this->product::all();
    }

    /**
     * Get all products by GeoData.
     * GeoData = [latitude, longitude, radius].
     *
     * @param $geoData
     * @return mixed
     */
    public function getByGeo($geoData)
    {
        return $this->product()->where(function($query) use ($geoData){
            list($latitude, $longitude, $radius) = $geoData;
            return $query->store()->geofence($latitude, $longitude, $radius);
        })->get();
    }

    /**
     * Find a specific Product by id.
     *
     * @param $id
     * @return mixed
     */
    public function find($id)
    {
        return $this->product->find($id);
    }


    /**
     * Store new Product instance.
     *
     * @param $data
     * @return mixed
     * @throws CreateException
     */
    public function create($data)
    {
        if ($product = $this->product::create($data)) {
            return $product;
        }
        throw new CreateException();
    }


    /**
     * Update an existing product.
     *
     * @param $product_id
     * @param $data
     * @return mixed
     * @throws UpdateException
     */
    public function update($product_id, $data)
    {
        $product = $this->product->findOrFail($product_id);

        if ($updated_product = $product->update($data)) {
            return $updated_product;
        }

        throw new UpdateException();
    }

    /**
     * Destroy an specified product.
     *
     * @param $product_id
     * @return mixed
     * @throws DeleteException
     */
    public function destroy($product_id)
    {
        $product = $this->product->findOrFail($product_id);

        if ($result = $product->delete($product_id)) {
            return $result;
        }

        throw new DeleteException();
    }
}
