<?php


namespace App\Repositories;


use App\Contracts\OrderRepositoryContract;
use App\Exceptions\OrderFailedException;

/**
 * Class OrderRepository
 * @package App\Repositories
 */
class OrderRepository implements OrderRepositoryContract
{
    /**
     * @var ProductRepository
     */
    private $product;
    /**
     * @var StoreRepository
     */
    private $store;
    /**
     * @var OrderRepository
     */
    private $order;

    /**
     * OrderRepository constructor.
     * @param ProductRepository $product
     * @param StoreRepository $store
     * @param OrderRepository $order
     */
    public function __construct(ProductRepository $product, StoreRepository $store, OrderRepository $order)
    {
        $this->product = $product;
        $this->store = $store;
        $this->order = $order;
    }

    /**
     * Buy new Product from a specific store.
     *
     * @param $user_id
     * @param $product_id
     * @param $store_id
     * @param int $quantity
     * @return mixed
     * @throws OrderFailedException
     */
    public function make($user_id, $product_id, $store_id, $quantity = 1)
    {
        $this->product()
            ->findOrFail($product_id)
            ->store()
            ->findOrFail($store_id);

        if ($new_order = $this->order->create($product_id, $store_id, $quantity)) {
            return $new_order;
        }

        throw new OrderFailedException();
    }
}
