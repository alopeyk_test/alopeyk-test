<?php


namespace App\Repositories;

use App\Contracts\StoreRepositoryContract;
use App\Exceptions\Store\CreateException;
use App\Exceptions\Store\DeleteException;
use App\Exceptions\Store\UpdateException;
use App\Store;

/**
 * Class StoreRepository
 * @package App\Repositories
 */
class StoreRepository implements StoreRepositoryContract
{
    /**
     * @var Store
     */
    private $store;

    /**
     * StoreRepository constructor.
     * @param Store $store
     */
    public function __construct(Store $store)
    {
        $this->store = $store;
    }

    /**
     * List all stores.
     *
     * @return Store[]
     */
    public function all() {
        return $this->store::all();
    }

    /**
     * List stores in a specific GeoData.
     * geoData = [latitude, longitude, radius].
     *
     * @param $geoData
     * @return mixed
     */
    public function getByGeo($geoData)
    {
        list($latitude, $longitude, $radius) = $geoData;

        return $this->store->geofence($latitude, $longitude, $radius);
    }

    /**
     * Find a specific Store.
     *
     * @param $id
     * @return mixed
     */
    public function find($id)
    {
        return $this->store->find($id);
    }

    /**
     * Create a new Store.
     *
     * @param $data
     * @return mixed
     * @throws CreateException
     */
    public function create($data)
    {
        if ($store = $this->store->create($data)) {
            return $store;
        }

        throw new CreateException();
    }

    /**
     * Update an existing Store.
     *
     * @param $store_id
     * @param $data
     * @return bool
     * @throws UpdateException
     */
    public function update($store_id, $data)
    {
        if ($store = $this->store->update($store_id, $data)) {
            return $store;
        }

        throw new UpdateException();
    }

    /**
     * Delete an specific store.
     *
     * @param $store_id
     * @return mixed
     * @throws DeleteException
     */
    public function delete($store_id)
    {
        $store = $this->store->findOrFail($store_id);

        if ($result = $store->destroy()) {
            return $result;
        }

        throw new DeleteException();
    }
}
