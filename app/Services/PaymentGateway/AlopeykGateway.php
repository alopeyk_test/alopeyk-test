<?php


namespace App\Services\PaymentGateway;


/**
 * Gateway Mock.
 *
 * Class AlopeykGateway
 * @package App\Services\PaymentGateway
 */
class AlopeykGateway
{
    /**
     * Create payment transaction and return transaction token.
     *
     * @param $order_id
     * @return string
     */
    public function create($order_id)
    {
        return str_random(20);
    }

    /**
     * Bank return message.
     *
     * @param bool $success
     * @return bool|mixed
     */
    public function pay($success = true)
    {
        return $success;
    }

    /**
     * Verify the payment.
     *
     * @param bool $success
     * @return bool|mixed
     */
    public function verify($success = true)
    {
        return $success;
    }
}
