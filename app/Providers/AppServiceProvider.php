<?php

namespace App\Providers;

use App\Contracts\OrderRepositoryContract;
use App\Contracts\ProductRepositoryContract;
use App\Contracts\StoreRepositoryContract;
use App\Repositories\OrderRepository;
use App\Repositories\ProductRepository;
use App\Repositories\StoreRepository;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            ProductRepositoryContract::class,
            ProductRepository::class
        );
        $this->app->bind(
            OrderRepositoryContract::class,
            OrderRepository::class
        );
        $this->app->bind(
            StoreRepositoryContract::class,
            StoreRepository::class
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }
}
